# jtranslate
Localize your apps with jtranslate.

This library provide to use [jinja2](http://jinja.pocoo.org/docs/2.9/) templates in strings and use [babel](http://babel.pocoo.org/en/latest/)

Locales files has simply YAML format.

## Installation
This library works only with Python>=3.6!

### PIP
```bash
pip install jtranslate
```

### setup.py
```bash
git clone https://bitbucket.org/illemius/jtranslator.git
cd jtranslator
python setup.py install
```

## API
- [Translator](#translator)
- [Translator Context](#translator-context)
- [DeleteItem](#deleteitem)
- [Locale](#locale)
- [yaml](#yaml)


### Translator
Base translator object

***Translator(path, default_locale, jinja_env=None)***

- **path**: Path to locales directory
- **default_locale**: Default locale name
- **jinja_env**: Instance of `jinja2.Environment` (Not required)

#### Properties
- **default_locale**: Default locale name
- **path**: Path to locales directory
- **locales**: List of loaded locales names
- **jinja_env**: Instance of `jinja2.Environment` (Not required)


#### Methods
- **load_all()**: Scan path and load all locales
- **reload_all()**: Update all loaded locales
- **save_all()**: Save all records
- **get_locale(locale_name)**: Get locale from already loaded or from disk by name
- **get_text(text, locale=None, context=None, formatted=True)**: Get text from dictionary
    - **text**: original text
    - **locale**: str or Locale
    - **context**: context for template
    - **formatted**: render jinja2 template
- **merge(save=True, clean=False)**: Merge keys in all loaded dictionaries
    - **save**: save on complete
    - **clean**: remove unused keys
- **update_globals(\*d, \*\*kwargs)**: Update jinja2 environment globals
- **\_\_call\_\_**: alias for `get_text`
- **\_\_getitem\_\_**: Return TranslatorContext


### Translator Context

***TranslatorContext(translator: Translator, locale: Locale)***

#### Methods
- **get_text(self, text, context=None, formatted=True)**: Alias for `get_text` but with loaded locale
- **\_\_call\_\_(self, text, context=None, formatted=True)**: Alias for `get_text` but with loaded locale


### Locale
Locale instance

***Locale(path, meta, strings, consts)***:
- **path**: File path
- **meta**: Meta information (Language name, territory, authors and etc.)
- **strings**: Translates
- **consts**: Consts for current locale

#### Properties
- **locale**: Return instance of `babel.Locale` configured for current locale
- **language_code**: Return language code (Get from filename)

#### Public methods
- *@classmethod* **from_file(path)**: Load locale from path
- *@classmethod* **new_locale(path)**: Create empty locale
- **save(force=False)**: Dump dictionary to file (Lazy method. Save only if `._changed` is True)
- **update()**: Load changes from FS.
- **get_text(text)**: Get text from dictionary
- **get_meta()**: Get meta info
- **get_consts()**: Get consts
- **start_merge()**: Initialize merging.
- **merge(key, value=None)**: Insert or remove records
- **\_\_enter\_\_()**: alias for `start_merge`
- **\_\_exit\_\_()**: Trigger save


### DeleteItem
You can use this object for deleting records: `translator.merge(key, DeleteItem())`


### yaml
utils for work with YAML format.