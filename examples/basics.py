import logging
import os

from jtranslate import Translator

PATH = os.path.split(os.path.abspath(__file__))[0]
LOCALES_PATH = os.path.join(PATH, 'locales')
translator = Translator(LOCALES_PATH, default_locale='en')

logging.basicConfig(level=logging.DEBUG)


def main():
    for locale in translator.locales:
        with translator[locale] as _:
            print(_('Use language: {{ META.language_name }} // {{ LOCALE.locale.english_name }}'))
            name = input(_('What is your name?') + ' ')
            print(_('Hi, {{ name }}!', context={'name': name}))


if __name__ == '__main__':
    translator.load_all()
    try:
        main()
    finally:
        translator.merge(False, True)
        translator.save_all()
